﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ПрР1_Джабурия;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ПрР1_Джабурия.Tests
{
    [TestClass()]
    public class MatrixTests
    {
        [TestMethod()]
        public void check_m_2_n_3_positiveElements_4()
        {
            int m = 2;
            int n = 3;

            int exepted = 4;

            int actual = Matrix.positiveElements(m, n);

            Assert.AreEqual(exepted, actual);
        }
    }
}